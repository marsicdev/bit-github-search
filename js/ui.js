const $searchInput = $('.search-input')
const $usersUl = $('.list')

const getInputText = () => $searchInput.val()

const displayUsers = (usersList) => {

    // clear previous input
    // $usersUl.html('')
    $usersUl.empty()

    // For each user in list create li element and add to ul
    for (let i = 0; i < usersList.length; i++) {
        const user = usersList[i];
        const $li = createLiElement(user)
        $usersUl.append($li)
    }
}

const createLiElement = (user) => {
    const { id, username } = user;
    return $(`<li>ID:${id} UN:${username}</li>`)
}

export {
    getInputText,
    displayUsers
}
