import User from './entities/User.js'

const BASE_ENDPOINT = 'https://api.github.com/search/users?q='

const fetchData = (searchInput, onSuccess) => {

    // const requestUrl = `https://api.github.com/search/users?q=${searchInput}`
    const requestUrl = BASE_ENDPOINT + searchInput

    $.ajax(requestUrl)
        .done((response) => {
            console.log("server response onDone", response);

            const usersArray = response.items

            // const myUsers = []
            // for (let i = 0; i < usersArray.length; i++) {
            //     const userData = usersArray[i];
            //     // const { id, login } = userData;

            //     const user = new User(userData.id, userData.login)
            //     myUsers.push(user)
            // }

            const myUsersMapped = usersArray.map(function (userData) {
                const { id, login } = userData;
                return new User(id, login)
            })


            onSuccess(myUsersMapped)
        })
}

export {
    fetchData
}