import * as ui from './ui.js'
import * as data from './data.js'

const $searchBtn = $('.search-btn')

const init = () => {
    console.log("App initialized");

    setupOnClickListeners()    
}

const setupOnClickListeners = () => {
    // Register search handler
    $searchBtn.on('click', onSearchClickHandler)
}

const onSearchClickHandler = (e) => {
    // Collect input search text
    const searchInput = ui.getInputText()

    // Fetch data with input text
    data.fetchData(searchInput, onSuccessResponseHandler)

}

const onSuccessResponseHandler = (usersArray) => {
    // display users on based on response data
    ui.displayUsers(usersArray)
}

export {
    init
}
